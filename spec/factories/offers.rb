# frozen_string_literal: true

FactoryBot.define do
  factory :offer, class: Offer do
    title { Faker::Name.name }
    description { Faker::Marketing.buzzwords }
    price { Faker::Number.between(1, 10) }

    # has_many
    trait :with_images do
      transient { images_count { 1 } }
      after(:create) do |offer, evaluator|
        create_list(:image, evaluator.images_count, offer: offer)
      end
    end

    factory :offer_with_images, traits: [:with_images]
  end

  factory :test_offer_model, class: Offer do
    title { 'Some Offer Name' }
    description { 'Some Description' }
    price { 10 }
  end
end
