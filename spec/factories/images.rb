# frozen_string_literal: true

FactoryBot.define do
  factory :image, class: Image do
    label { 'Some Image Label' }
    image_data { 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mP8/5+hHgAHggJ/PchI7wAAAABJRU5ErkJggg==' }
    image_path { 'http://127.0.0.1:3000/offers/15/offer_image?file=offer_15_image_a7417c03062907d35355.png' }
    image_name { 'offer_15_image_a7417c03062907d35355.png' }
    content_type { 'image/png' }

    association :offer, factory: :offer
  end

  factory :test_image_model, class: Image do
    label { 'Some Image Label' }
    image_data { 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mP8/5+hHgAHggJ/PchI7wAAAABJRU5ErkJggg==' }
    image_path { 'http://127.0.0.1:3000/offers/15/offer_image?file=offer_15_image_a7417c03062907d35355.png' }
    image_name { 'offer_15_image_a7417c03062907d35355.png' }
    content_type { 'image/png' }
  end
end
