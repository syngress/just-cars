# frozen_string_literal: true

require 'rails_helper'

describe OffersController, type: :routing do
  it { expect(get: '/offers').to route_to('offers#index') }
  it { expect(post: '/offers').to route_to('offers#create') }
  it { expect(get: '/offers/1').to route_to('offers#show', id: '1') }
  it { expect(put: '/offers/1').to route_to('offers#update', id: '1') }
end
