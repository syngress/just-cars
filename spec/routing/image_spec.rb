# frozen_string_literal: true

require 'rails_helper'

describe ImagesController, type: :routing do
  it { expect(get: '/offers/1/images').to route_to('images#index', offer_id: '1') }
  it { expect(post: '/offers/1/images').to route_to('images#create', offer_id: '1') }
  it { expect(get: '/images/1').to route_to('images#show', id: '1') }
end
