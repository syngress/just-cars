# frozen_string_literal: true

require 'rails_helper'

describe Images::CreateImageService, type: :service do
  let(:offer) { create(:offer_with_images) }
  subject do
    form.objects[:offer] = offer
    described_class.new(
      form: form,
      offer: offer
    ).call
  end

  describe '#call' do
    context 'when image form is valid' do
      let(:form) { Images::CreateImageForm.new(attributes_for(:image)) }
      it 'verifies if the reservation has been saved in the database' do
        subject
        expect(offer.images.count).to be > 1
      end
      it 'checks for valid class instance' do
        is_expected.to be_an_instance_of(ServiceResult)
      end
    end

    context 'when restaurant form is invalid' do
      let(:form) { Images::CreateImageForm.new(a: 10, b: 12) }
      it 'checks for valid class instance' do
        expect(subject.object).to be_an_instance_of(Error::ValidationFailed)
      end
    end
  end
end
