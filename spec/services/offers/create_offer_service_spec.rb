# frozen_string_literal: true

require 'rails_helper'

describe Offers::CreateOfferService, type: :service do
  subject { described_class.new(form: form).call }

  describe '#call' do
    context 'when offer form is valid' do
      let(:form) { Offers::CreateOfferForm.new(attributes_for(:offer)) }
      it 'perform form value validation' do
        expect(subject.object.title).to eql(form.title)
        expect(subject.object.description).to eql(form.description)
        expect(subject.object.price).to eql(form.price)
      end
      it 'verifies if the offer has been saved in the database' do
        subject
        expect(Offer.count).to eql(1)
      end
      it 'checks for valid class instance' do
        is_expected.to be_an_instance_of(ServiceResult)
      end
    end

    context 'when offer form is invalid' do
      let(:form) { Offers::CreateOfferForm.new({}) }
      it {
        expect(subject.object).to be_an_instance_of(Error::ValidationFailed)
        expect(Offer.count).to eql(0)
      }
    end
  end
end
