# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Error::ValidationFailed do
  let(:details) { 'Some error details' }
  subject { Error::ValidationFailed.new(details) }

  it { expect(subject.key).to eq(:validation_failed) }
  it { expect(subject.http_status.to_i).to eq(400) }
  it { expect(subject.details).to eq(details) }
end
