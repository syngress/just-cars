# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Error::InternalServerError do
  let(:details) { 'Some error details' }
  subject { Error::InternalServerError.new(details) }

  it { expect(subject.key).to eq(:internal_server_error) }
  it { expect(subject.http_status.to_i).to eq(500) }
  it { expect(subject.details).to eq(details) }
end
