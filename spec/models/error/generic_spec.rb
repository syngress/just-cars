# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Error::Generic do
  let(:message) { 'Some generic error message' }
  let(:key) { :test_key }
  let(:http_status) { 400 }
  subject { Error::Generic.new(message, key, http_status) }

  it { expect(subject.key).to eq(:test_key) }
  it { expect(subject.http_status.to_i).to eq(400) }
  it { expect(subject.message).to eq(message) }
end
