# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Error::Forbidden do
  let(:message) { 'Access forbidden for this token' }
  subject { Error::Forbidden.new(message) }

  it { expect(subject.key).to eq(:forbidden) }
  it { expect(subject.http_status.to_i).to eq(403) }
  it { expect(subject.message).to eq(message) }
end
