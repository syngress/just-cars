# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Error::InvalidAction do
  let(:options) { 'Soma invalid action details' }
  subject { Error::InvalidAction.new(details: options) }

  it { expect(subject.key).to eq(:invalid_action) }
  it { expect(subject.http_status.to_i).to eq(400) }
  it { expect(subject.details).to eq(options) }
end
