# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Error::InvalidParameter do
  let(:parameter) { 'sort_by'.try(:to_sym) }
  let(:options) { { value: { sort_by: 1234 } } }
  let(:message) { 'Provided unsupported value ({:sort_by=>1234}) to parameter (sort_by).' }
  subject { Error::InvalidParameter.new(parameter, options) }

  it { expect(subject.key).to eq(:invalid_parameter) }
  it { expect(subject.http_status.to_i).to eq(400) }
  it { expect(subject.message).to eq(message) }
end
