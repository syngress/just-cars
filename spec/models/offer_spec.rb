# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Offer, type: :model do
  describe '.superclass' do
    subject { described_class.superclass }
    it { is_expected.to eq(ApplicationRecord) }
  end

  describe '.column_names' do
    subject { described_class.column_names }
    it { is_expected.to include('id') }
    it { is_expected.to include('title') }
    it { is_expected.to include('description') }
    it { is_expected.to include('price') }
  end

  describe '.new' do
    subject { build(:test_offer_model) }
    it { is_expected.to be_a(described_class) }
    it 'assigns attributes properly' do
      expect(subject.title).to eq('Some Offer Name')
      expect(subject.description).to eq('Some Description')
      expect(subject.price).to be_within(0.1).of(10.0)
    end
  end

  describe '#title' do
    subject { build(:test_offer_model).title }
    it { is_expected.to be_a(String) }
    it { is_expected.to eq('Some Offer Name') }
  end

  describe '#description' do
    subject { build(:test_offer_model).description }
    it { is_expected.to be_a(String) }
    it { is_expected.to eq('Some Description') }
  end

  describe '#price' do
    subject { build(:test_offer_model).price }
    it { should be_a_kind_of(Numeric) }
    it { should be_within(0.1).of(10.0) }
    it { should_not be_within(20.0).of(100.0) }
  end

  # Validation tests
  it { should validate_presence_of(:title) }
  it { should validate_presence_of(:description) }
  it { should validate_presence_of(:price) }
end
