# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Image, type: :model do
  describe '.superclass' do
    subject { described_class.superclass }
    it { is_expected.to eq(ApplicationRecord) }
  end

  describe '.column_names' do
    subject { described_class.column_names }
    it { is_expected.to include('id') }
    it { is_expected.to include('label') }
    it { is_expected.to include('image_data') }
    it { is_expected.to include('image_path') }
    it { is_expected.to include('image_name') }
    it { is_expected.to include('content_type') }
  end

  describe '.new' do
    subject { build(:test_image_model) }
    it { is_expected.to be_a(described_class) }
    it 'assigns attributes properly' do
      expect(subject.label).to eq('Some Image Label')
      expect(subject.image_data).to eq('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mP8/5+hHgAHggJ/PchI7wAAAABJRU5ErkJggg==')
      expect(subject.image_path).to eq('http://127.0.0.1:3000/offers/15/offer_image?file=offer_15_image_a7417c03062907d35355.png')
      expect(subject.image_name).to eq('offer_15_image_a7417c03062907d35355.png')
      expect(subject.content_type).to eq('image/png')
    end
  end

  describe '#label' do
    subject { build(:test_image_model).label }
    it { is_expected.to be_a(String) }
    it { is_expected.to eq('Some Image Label') }
  end

  describe '#image_data' do
    subject { build(:test_image_model).image_data }
    it { is_expected.to be_a(String) }
    it { is_expected.to eq('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mP8/5+hHgAHggJ/PchI7wAAAABJRU5ErkJggg==') }
  end

  describe '#image_path' do
    subject { build(:test_image_model).image_path }
    it { is_expected.to be_a(String) }
    it { is_expected.to eq('http://127.0.0.1:3000/offers/15/offer_image?file=offer_15_image_a7417c03062907d35355.png') }
  end

  describe '#image_name' do
    subject { build(:test_image_model).image_name }
    it { is_expected.to be_a(String) }
    it { is_expected.to eq('offer_15_image_a7417c03062907d35355.png') }
  end

  describe '#content_type' do
    subject { build(:test_image_model).content_type }
    it { is_expected.to be_a(String) }
    it { is_expected.to eq('image/png') }
  end

  # Validation tests
  it { should validate_presence_of(:label) }
  it { should validate_presence_of(:image_data) }
  it { should validate_presence_of(:image_path) }
  it { should validate_presence_of(:image_name) }
  it { should validate_presence_of(:content_type) }
end
