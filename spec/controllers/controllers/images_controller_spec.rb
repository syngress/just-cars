# frozen_string_literal: true

require 'rails_helper'

describe ImagesController, type: :controller do
  context 'when images exists' do
    let(:offer) { create(:offer_with_images) }

    # GET list of images
    describe 'GET #index' do
      it 'responds successfully with an HTTP 200 status code' do
        get :index, params: { offer_id: offer.id }
        expect(response).to be_successful
        expect(response).to have_http_status(200)
      end
    end
    # GET image object
    describe 'GET #show' do
      it 'responds successfully with an HTTP 200 status code' do
        get :show, params: { id: offer.images.first.id }
        expect(response).to be_successful
        expect(response).to have_http_status(200)
      end
    end
  end

  context 'when images does not exists' do
    let(:offer) { create(:offer_with_images) }

    # POST image object
    describe 'POST #create' do
      it 'responds successfully with an HTTP 200 status code' do
        post :create, params: { offer_id: offer.id }.merge(FactoryBot.attributes_for(:image))
        expect(response).to be_successful
        expect(response).to have_http_status(200)
      end
    end
  end
end
