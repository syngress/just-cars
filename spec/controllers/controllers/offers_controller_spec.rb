# frozen_string_literal: true

require 'rails_helper'

describe OffersController, type: :controller do
  context 'when offer exists' do
    let(:offer) { create(:offer) }
    # Get list of restaurant's
    describe 'GET #index' do
      it 'responds successfully with an HTTP 200 status code' do
        get :index
        expect(response).to be_successful
        expect(response).to have_http_status(200)
      end
    end
    # GET restaurant object
    describe 'GET #show' do
      it 'responds successfully with an HTTP 200 status code' do
        get :show, params: { id: offer.id }
        expect(response).to be_successful
        expect(response).to have_http_status(200)
      end
    end
    # PUT restaurant object
    describe 'PUT #update' do
      it 'responds successfully with an HTTP 200 status code' do
        put :update, params: { id: offer.id, title: 'Some New Title' }
        expect(response).to be_successful
        expect(response).to have_http_status(200)
      end
    end
  end

  context 'when offer does not exists' do
    # Create offer object
    describe 'POST #create' do
      it 'responds successfully with an HTTP 200 status code' do
        post :create, params: FactoryBot.attributes_for(:offer)
        expect(response).to be_successful
        expect(response).to have_http_status(200)
        expect(Offer.count).to eql(1)
      end
    end
  end
end
