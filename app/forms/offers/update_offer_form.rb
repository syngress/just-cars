# frozen_string_literal: true

module Offers
  # app/forms/offers/update_offer_form.rb
  class UpdateOfferForm < Form
    attribute :title, String
    attribute :description, String
    attribute :price, Decimal

    validates :title,
              length: { maximum: 255 },
              if: -> { self.keys.include?(:title) }

    validates :description,
              length: { maximum: 255 },
              if: -> { self.keys.include?(:description) }

    validates :price,
              length: { maximum: 255 },
              numericality: { only_decimal: true, greater_than: 0 },
              if: -> { self.keys.include?(:price) }
  end
end
