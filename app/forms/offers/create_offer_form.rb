# frozen_string_literal: true

module Offers
  # app/forms/offers/create_offer_form.rb
  class CreateOfferForm < Form
    attribute :title, String
    attribute :description, String
    attribute :price, Decimal

    validates :title,
              presence: true

    validates :description,
              presence: true

    validates :price,
              presence: true,
              numericality: { only_decimal: true, greater_than: 0 }
  end
end
