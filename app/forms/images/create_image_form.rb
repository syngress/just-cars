# frozen_string_literal: true

module Images
  # app/forms/images/crate_image_form.rb
  class CreateImageForm < Form
    attribute :label, String
    attribute :image_data, String

    validates :label,
              presence: true

    validates :image_data,
              presence: true
  end
end
