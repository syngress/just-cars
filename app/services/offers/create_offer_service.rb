# frozen_string_literal: true

module Offers
  # app/services/offers/create_offer_service.rb
  class CreateOfferService
    include Service

    attr_reader :form

    def initialize(attrs = {})
      @form = attrs.fetch(:form)
    end

    def call
      return build_result(validation_error) unless form.valid?

      build_result(offer)
    end

    private

    def offer
      @offer ||= Offer.create(form.to_hash)
    end
  end
end
