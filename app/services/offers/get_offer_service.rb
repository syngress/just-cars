# frozen_string_literal: true

module Offers
  # app/services/offers/get_offer_service.rb
  class GetOfferService
    include Service

    attr_reader :sort_by, :sort_order, :page, :per_page

    def initialize(attrs = {})
      @sort_by = attrs.fetch(:sort_by)
      @sort_order = attrs.fetch(:sort_order)
      @page = attrs.fetch(:page)
      @per_page = attrs.fetch(:per_page)
      @params = attrs.fetch(:params)
    end

    def call
      collect_offers
    end

    private

    def collect_offers
      Offer.all.order(order).page(page).per(per_page)
    end

    def order
      { sort_by => sort_order }
    end
  end
end
