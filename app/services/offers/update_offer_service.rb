# frozen_string_literal: true

module Offers
  # app/services/offers/update_offer_service.rb
  class UpdateOfferService
    include Service

    attr_reader :form, :offer

    def initialize(attrs = {})
      @form = attrs.fetch(:form)
      @offer = attrs.fetch(:offer)
    end

    def call
      return build_result(validation_error) unless form.valid?

      offer.update_attributes(form.to_hash)
      build_result(offer)
    end
  end
end
