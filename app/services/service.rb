# frozen_string_literal: true

# /app/services/service.rb
module Service
  extend ActiveSupport::Concern
  # Main service module
  module ClassMethods
    def call(attrs = {})
      new(attrs).call
    end
  end

  private

  attr_reader :form

  def validation_error
    Error::ValidationFailed.new(form.errors)
  end

  def build_result(object = nil)
    ServiceResult.new(object)
  end
end
