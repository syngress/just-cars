# frozen_string_literal: true

module Images
  # app/services/images/get_image_service.rb
  class GetImageService
    include Service

    attr_reader :sort_by, :sort_order, :page, :per_page, :params

    def initialize(attrs = {})
      @sort_by = attrs.fetch(:sort_by)
      @sort_order = attrs.fetch(:sort_order)
      @page = attrs.fetch(:page)
      @per_page = attrs.fetch(:per_page)
      @params = attrs.fetch(:params)
    end

    def call
      collect_images
    end

    private

    def collect_images
      images = Image.where(offer_id: params[:offer_id]).order(order).page(page).per(per_page)
      images.empty? ? (raise Error::ResourceNotFound) : images
    end

    def order
      { sort_by => sort_order }
    end
  end
end
