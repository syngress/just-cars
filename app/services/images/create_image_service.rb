# frozen_string_literal: true

module Images
  # app/services/images/create_image_service.rb
  class CreateImageService
    require 'fileutils'
    include Service

    BASE_REGEXP = %r{\Adata:([-\w]+/[-\w\+\.]+)?;base64,(.*)}m.freeze

    attr_reader :offer, :form, :file_path

    def initialize(attrs = {})
      @form = attrs.fetch(:form)
      @offer = attrs.fetch(:offer)
      @file_path = Settings.image_configuration.path + offer.id.to_s
    end

    def call
      return build_result(validation_error) unless form.valid?

      create_image_directory
      build_result(create_image)
    end

    private

    def create_image_directory
      Dir.mkdir(File.join(Rails.root, file_path)) unless Dir.exist?(File.join(Rails.root, file_path))
    end

    def create_image
      image_data_parts = form.image_data.match(BASE_REGEXP) || []
      content_type = image_data_parts[1].gsub('image/', '')
      file_name = "offer_#{offer.id}_image_#{SecureRandom.hex(10)}.#{content_type}"
      compressed_base64 = Zlib::Deflate.deflate(form.image_data)
      image_url = (
        Settings.image_configuration.domain +
        "/offers/#{offer.id}/offer_image?file=#{file_name}"
      )
      # We can return origin Base64 everywhere in app, for example:
      # Zlib::Inflate.inflate(Base64.decode64(offer.images.first.image_data)
      process_data(file_name, image_data_parts, compressed_base64, image_url)
    end

    def process_data(file_name, image_data_parts, compressed_base64, image_url)
      File.open(File.join(Rails.root, file_path, file_name), 'wb') do |file|
        file.write(Base64.decode64(image_data_parts[2]))
      end

      image_params = form.to_hash.merge!(
        image_path: image_url,
        image_name: file_name,
        image_data: Base64.encode64(compressed_base64),
        content_type: image_data_parts[1]
      )

      offer.images.create(image_params)
    end
  end
end
