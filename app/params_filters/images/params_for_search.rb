# frozen_string_literal: true

module Images
  # app/params_filters/images/params_for_search.rb
  class ParamsForSearch
    extend ParamsFilter
    class << self
      def allowed_attributes
        %i[id label image_name image_path content_type]
      end
    end
  end
end
