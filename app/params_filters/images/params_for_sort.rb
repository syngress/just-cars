# frozen_string_literal: true

module Images
  # app/params_filters/images/params_for_sort.rb
  class ParamsForSort
    extend ParamsFilter

    class << self
      def allowed_attributes
        %i[sort_by sort_order]
      end

      def allowed_values
        %i[id label content_type]
      end
    end
  end
end
