# frozen_string_literal: true

module Offers
  # app/params_filters/offers/params_for_search.rb
  class ParamsForSearch
    extend ParamsFilter
    class << self
      def allowed_attributes
        %i[id title description price]
      end
    end
  end
end
