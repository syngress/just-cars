# frozen_string_literal: true

# models/offer.rb
class Offer < ApplicationRecord
  # model association
  has_many :images, class_name: 'Image', foreign_key: :offer_id, dependent: :destroy

  # validations
  validates_presence_of :title, :description, :price
end
