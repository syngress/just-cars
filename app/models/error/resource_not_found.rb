# frozen_string_literal: true

class Error
  # /app/models/error/resource_not_found.rb
  class ResourceNotFound < Error
    def initialize(message = nil)
      super(message || origin_message, :resource_not_found, {}, Http::Status.new(404))
    end

    def origin_message
      I18n.t :resource_not_found, scope: 'errors.messages'
    end
  end
end
