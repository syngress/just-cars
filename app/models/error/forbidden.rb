# frozen_string_literal: true

class Error
  # /app/models/error/forbidden.rb
  class Forbidden < Error
    def initialize(message = nil)
      super(message || origin_message, :forbidden, {}, Http::Status.new(403))
    end

    def origin_message
      I18n.t :forbidden, scope: 'errors.messages'
    end
  end
end
