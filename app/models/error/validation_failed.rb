# frozen_string_literal: true

class Error
  # /app/models/error/validation_failed.rb
  class ValidationFailed < Error
    def initialize(details = nil, _data = {})
      message = I18n.t :validation_failed, scope: 'errors.messages'
      super(message, :validation_failed, details, Http::Status.new(400))
    end
  end
end
