# frozen_string_literal: true

class Error
  # app/models/error/routing_not_found.rb
  class RoutingNotFound < Error
    def initialize(request)
      super("No route matches [#{request.request_method}] #{request.path}", :routing_not_found, {}, Http::Status.new(404))
    end
  end
end
