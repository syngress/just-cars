# frozen_string_literal: true

# app/models/image.rb
class Image < ApplicationRecord
  belongs_to :offer

  # validations
  validates_presence_of :label, :image_data, :image_path, :image_name, :content_type
end
