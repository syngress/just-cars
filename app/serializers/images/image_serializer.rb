# frozen_string_literal: true

module Images
  # app/serializers/images/image_serializer.rb
  class ImageSerializer < ActiveModel::Serializer
    attributes %i[id offer_id label image_path image_name content_type created_at updated_at]
  end
end
