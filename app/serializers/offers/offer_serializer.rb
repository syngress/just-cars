# frozen_string_literal: true

module Offers
  # /app/serializers/offers/offer_serializer.rb
  class OfferSerializer < ActiveModel::Serializer
    attributes %i[id title description price images created_at updated_at]

    has_many :images, serializer: Images::ImageSerializer
  end
end
