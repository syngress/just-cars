# frozen_string_literal: true

# /app/serializers/error_serializer.rb
class ErrorSerializer < ActiveModel::Serializer
  attributes :status, :error, :message, :details

  def error
    object.key
  end

  def status
    object.http_status.to_i
  end
end
