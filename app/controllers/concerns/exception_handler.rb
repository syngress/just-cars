# frozen_string_literal: true

# app/controllers/concerns/exception_handler.rb
module ExceptionHandler
  extend ActiveSupport::Concern

  # Define custom error subclasses - rescue catches
  class AuthenticationError < StandardError; end

  included do
    # Define custom handlers
    rescue_from ActiveRecord::RecordInvalid, with: :invalid_request

    rescue_from ActiveRecord::RecordNotFound do |error|
      json_response({ message: error.message }, :not_found)
    end
  end

  private

  # JSON response with message; Status code 422 - unprocessable entity
  def invalid_request(error)
    json_response({ message: error.message }, :unprocessable_entity)
  end
end
