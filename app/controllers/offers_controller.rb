# frozen_string_literal: true

# app/controllers/offers_controller.rb
class OffersController < ApplicationController
  include Pagination
  include Sorting

  before_action :offer, only: %i[show update]

  def show
    render json: offer, serializer: Offers::OfferSerializer
  end

  def index
    setup_pagination(page: params[:page], per_page: params[:per_page])
    setup_sorting(sort_by: params[:sort_by], sort_order: params[:sort_order])
    render json: {
      recordsTotal: offers.count,
      data: ActiveModel::Serializer::CollectionSerializer.new(
        offers,
        serializer: Offers::OfferSerializer
      )
    }
  end

  def create
    form = Offers::CreateOfferForm.new(offer_params)
    result = Offers::CreateOfferService.call(form: form)
    if result.error?
      render_error(result.object)
    else
      render json: result.object, serializer: Offers::OfferSerializer
    end
  end

  def update
    form = Offers::UpdateOfferForm.new(offer_params, offer: offer)
    result = Offers::UpdateOfferService.call(form: form, offer: offer)
    if result.error?
      render_error(result.object)
    else
      render json: result.object, serializer: Offers::OfferSerializer
    end
  end

  def offer_image
    target = File.join(Rails.root, Settings.image_configuration.path, params['id'], params['file'])
    if File.exist?(target)
      send_file(target, disposition: 'inline')
    else
      render json: { message: 'file not found' }
    end
  end

  private

  def offer
    @offer ||= Offer.find(params[:id])
  end

  def offers
    @offers ||= Offers::GetOfferService.call(
      params: params,
      page: page,
      per_page: per_page,
      sort_by: sort_by,
      sort_order: sort_order
    )
  end

  def all_offers
    Offer.all.order(order).page(page).per(per_page)
  end

  def sort_by_parameter
    Offers::ParamsForSort.allowed_values
  end

  def offer_params
    params.permit(:id, :title, :description, :price).to_h
  end

  def params_to_update
    FilterUpdateParams.call(object: offer, params: offer_params)
  end
end
