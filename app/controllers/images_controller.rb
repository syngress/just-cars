# frozen_string_literal: true

# app/controllers/images_controller.rb
class ImagesController < ApplicationController
  include Pagination
  include Sorting

  before_action :image, only: [:show]

  def show
    render json: image, serializer: Images::ImageSerializer
  end

  def index
    setup_pagination(page: params[:page], per_page: params[:per_page])
    setup_sorting(sort_by: params[:sort_by], sort_order: params[:sort_order])
    render json: {
      recordsTotal: images.count,
      data: ActiveModel::Serializer::CollectionSerializer.new(
        images,
        serializer: Images::ImageSerializer
      )
    }
  end

  def create
    form = Images::CreateImageForm.new(image_params)
    result = Images::CreateImageService.call(form: form, offer: offer)
    if result.error?
      render_error(result.object)
    else
      render json: result.object, serializer: Images::ImageSerializer
    end
  end

  private

  def offer
    @offer ||= Offer.find(params[:offer_id])
  end

  def image
    @image ||= Image.find(params[:id])
  end

  def images
    @images ||= Images::GetImageService.call(
      params: params,
      page: page,
      per_page: per_page,
      sort_by: sort_by,
      sort_order: sort_order
    )
  end

  def sort_by_parameter
    Images::ParamsForSort.allowed_values
  end

  def image_params
    params.permit(:id, :label, :image_data).to_h
  end

  def params_to_update
    FilterUpdateParams.call(object: offer, params: offer_params)
  end
end
