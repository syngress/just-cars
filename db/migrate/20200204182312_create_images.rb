# frozen_string_literal: true

# db/migrate/20200204182312_create_images.rb
class CreateImages < ActiveRecord::Migration[5.2]
  def change
    create_table :images do |t|
      t.string :label
      t.string :image_data
      t.string :image_path
      t.string :image_name
      t.string :content_type
      t.references :offer, foreign_key: true

      t.timestamps
    end
  end
end
