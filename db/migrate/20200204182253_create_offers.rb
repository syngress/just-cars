# frozen_string_literal: true

# db/migrate/20200204182253_create_offers.rb
class CreateOffers < ActiveRecord::Migration[5.2]
  def change
    create_table :offers do |t|
      t.string :title
      t.string :description
      t.decimal :price

      t.timestamps
    end
  end
end
