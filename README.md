# JustCars
![RubyOnRailsIMG](https://logo.clearbit.com/rubyonrails.org)

# Car sales offers application

Just Cars application, displays car sales offers.  
Offer should consist of:

  - car photo
  - the title of the offer
  - description of the offer
  - car price  

Application should have a REST API for creating and listing offers, picking a single offer.  
The functionality should include filter and pagination of the API response.  
Offer image can be saved in any project location configured from ```config/settings.yml```  
In response application will return *path* with proper url to the created file.   
Based on *path* front application will be able to show image.  
Base64 image should be heavily compressed and saved in the PG database, but should not be returned in json response, whe have image generated instead !  
Application should allow Base64 to be returned from the model level.  
Front application validate graphic files content-type which will be sent to the API.  
Use Service, Form , Query and View Objects (Serializers) design patterns.

### Table of endpoint  
| Method | Endpoint                                   |Note                      |
| ------ | ------------------------------------------ |------------------------- |
| POST   | /offers                                    | Create Offer             |
| GET    | /offers                                    | Get Offers Collection    |
| GET    | /offers/:id                                | Get Offer Object         |
| PUT    | /offers/:id                                | Update Offer Object      |  
| ------ | ------------------------------------------ |------------------------- |
| POST   | /offers/:id/images                         | Create Offer Image       |
| GET    | /offers/:id/images                         | Get Images Collection    |
| GET    | /images/:id                                | Get Image Object         |  

# Request Examples  
### CREATE OFFER
###  
```
[Create new offer request]
curl -X POST http://localhost:3000/offers -H 'Content-Type: application/json' -H 'cache-control: no-cache' -d '

{
	"title": "Fiat Duplo 1.8 Diesel",
	"description": "Some super description about my car..",
	"price": 12345
}

[Create new offer response]
{
    "id": 10,
    "title": "Fiat Duplo 1.8 Diesel",
    "description": "Some super description about my car..",
    "price": "12345.0",
    "images": [],
    "created_at": "2020-02-06T23:04:37.883+01:00",
    "updated_at": "2020-02-06T23:04:37.883+01:00"
}
```  
### GET OFFER COLLECTION
###
```
[Get offer collection request]
curl -X GET http://localhost:3000/offers -H 'Content-Type: application/json'

[Get offer collection response]
{
    "recordsTotal": 1,
    "data": [
        {
            "id": 11,
            "title": "Fiat Duplo 1.8 Diesel",
            "description": "Some super description about my car..",
            "price": "12345.0",
            "images": [
                {
                    "id": 18,
                    "offer_id": 11,
                    "label": "SomeIMageName2",
                    "image_path": "http://127.0.0.1:3000/offer_image/11/images?file=offer_11_image_dcf9916abfad5424653a.png",
                    "image_name": "offer_11_image_2e42263e7fcd7c4bb622.png",
                    "content_type": "image/png",
                    "created_at": "2020-02-06T23:08:00.022+01:00",
                    "updated_at": "2020-02-06T23:08:00.022+01:00"
                },
                {
                    "id": 19,
                    "offer_id": 11,
                    "label": "SomeIMageName3",
                    "image_path": "http://127.0.0.1:3000/offer_image/11/images?file=offer_11_image_dcf9916abfad542465ra.png",
                    "image_name": "offer_11_image_85be980d25f5cbe592ac.png",
                    "content_type": "image/png",
                    "created_at": "2020-02-06T23:08:13.986+01:00",
                    "updated_at": "2020-02-06T23:08:13.986+01:00"
                }
            ],
            "created_at": "2020-02-06T23:07:42.685+01:00",
            "updated_at": "2020-02-06T23:07:42.685+01:00"
        }
    ]
}
```  
### GET OFFER OBJECT
###
```
[Get offer object request]
curl -X GET http://localhost:3000/offers/1 -H 'Content-Type: application/json'

[Get offer object response]
{
    "id": 11,
    "title": "Fiat Duplo 1.8 Diesel",
    "description": "Some super description about my car..",
    "price": "12345.0",
    "images": [
        {
            "id": 18,
            "offer_id": 11,
            "label": "SomeIMageName2",
            "image_path": "http://127.0.0.1:3000/offer_image/11/images?file=offer_11_image_dcf9916abfad5424653a.png",
            "image_name": "offer_11_image_2e42263e7fcd7c4bb622.png",
            "content_type": "image/png",
            "created_at": "2020-02-06T23:08:00.022+01:00",
            "updated_at": "2020-02-06T23:08:00.022+01:00"
        },
        {
            "id": 19,
            "offer_id": 11,
            "label": "SomeIMageName3",
            "image_path": "http://127.0.0.1:3000/offer_image/11/images?file=offer_11_image_dcf9916abfad542465ra.png",
            "image_name": "offer_11_image_85be980d25f5cbe592ac.png",
            "content_type": "image/png",
            "created_at": "2020-02-06T23:08:13.986+01:00",
            "updated_at": "2020-02-06T23:08:13.986+01:00"
        }
    ],
    "created_at": "2020-02-06T23:07:42.685+01:00",
    "updated_at": "2020-02-06T23:07:42.685+01:00"
}
```  
### UPDATE OFFER OBJECT
###
```
[Update offer object request]
curl -X POST http://localhost:3000/offers/1 -H 'Content-Type: application/json' -H 'cache-control: no-cache' -d '
{
    "title": "Fiat Duplo 1.8 Diesel",
    "description": "Some super description about my car..",
    "price": "12345.0"
}

[Update offer object response]
{
    "id": 10,
    "title": "Fiat Duplo 1.8 Diesel",
    "description": "Some super description about my car..",
    "price": "12345.0",
    "images": [],
    "created_at": "2020-02-06T23:04:37.883+01:00",
    "updated_at": "2020-02-06T23:04:37.883+01:00"
}
```  
### CREATE OFFER IMAGE
###
```
[Create new offer image request]
curl -X POST http://localhost:3000/offers/3/images -H 'Content-Type: application/json' -H 'cache-control: no-cache' -d '

{
	"label": "SomeIMageName2",
	"image_data": "Some_Base64"
}

[Create new offer image response]
{
    "id": 19,
    "offer_id": 11,
    "label": "SomeIMageName3",
    "image_path": "http://127.0.0.1:3000/offer_image/11/images?file=offer_11_image_dcf9916abfad5424653a.png",
    "image_name": "offer_11_image_85be980d25f5cbe592ac.png",
    "content_type": "image/png",
    "created_at": "2020-02-06T23:08:13.986+01:00",
    "updated_at": "2020-02-06T23:08:13.986+01:00"
}
```  
### GET OFFER IMAGE COLLECTION
###
```
[Get image collection request]
curl -X GET http://localhost:3000/offers/3/images -H 'Content-Type: application/json'

[Get image collection response]
{
    "recordsTotal": 2,
    "data": [
        {
            "id": 18,
            "offer_id": 11,
            "label": "SomeIMageName2",
            "image_path": "http://127.0.0.1:3000/offer_image/11/images?file=offer_11_image_dcf9916abfad5424653a.png",
            "image_name": "offer_11_image_2e42263e7fcd7c4bb622.png",
            "content_type": "image/png",
            "created_at": "2020-02-06T23:08:00.022+01:00",
            "updated_at": "2020-02-06T23:08:00.022+01:00"
        },
        {
            "id": 19,
            "offer_id": 11,
            "label": "SomeIMageName3",
            "image_path": "http://127.0.0.1:3000/offer_image/11/images?file=offer_11_image_dcf9916abfad542465ba.png",
            "image_name": "offer_11_image_85be980d25f5cbe592ac.png",
            "content_type": "image/png",
            "created_at": "2020-02-06T23:08:13.986+01:00",
            "updated_at": "2020-02-06T23:08:13.986+01:00"
        }
    ]
}
```  
### GET OFFER IMAGE OBJECT
###
```
[Get image object request]
curl -X GET http://localhost:3000/images/1 -H 'Content-Type: application/json'

[Get image object response]
{
    "id": 18,
    "offer_id": 11,
    "label": "SomeIMageName2",
    "image_path": "http://127.0.0.1:3000/offer_image/11/images?file=offer_11_image_dcf9916abfad5424653a.png",
    "image_name": "offer_11_image_2e42263e7fcd7c4bb622.png",
    "content_type": "image/png",
    "created_at": "2020-02-06T23:08:00.022+01:00",
    "updated_at": "2020-02-06T23:08:00.022+01:00"
}
```  

# Translations

Application enables translation of returned messages into two languages (Polish and English).  
We have two option ```pl_PL``` and ```en_GB```    
All you need to do is send the following values in the request headers:  
###
```
Accept-Language:pl_PL
```  

# Errors  

Just Cars application has its own error handling process.  
Among many HTTP error codes you can add and handle your own additional HTTP error codes.  
HTTP code must appear in [RFC6231](https://tools.ietf.org/html/rfc7231#section-6.5.1)  
