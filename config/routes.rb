# frozen_string_literal: true

Rails.application.routes.draw do
  match '/', to: 'application#info', via: [:get]
  match '/', to: 'application#routing_not_found', via: %i[post put]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :offers, only: %i[index show create update] do
    resources :images, only: %i[index show create], shallow: true
    get :offer_image, on: :member
  end
  match '*path', to: 'application#routing_not_found', via: :all
end
